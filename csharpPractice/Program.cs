﻿using System;

namespace csharpPractice
{
    class Program
    {
        // object is an instance of a class

        // csharp programs start executing with the main function; entry point to the program
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            
            Book book1 = new Book("harry potter", "success john", "PG-13");
            book1.Author = "dara John";
            Console.WriteLine(sizeof(double));

            /*
            Book book2 = new Book("half of a yellow sun", "chimamanda Adichie", "PG");

            Console.WriteLine(book1.Authors);


            

            Console.WriteLine(Book.Song);

            fullThrottle();

            halfThrottle();
            */
            // method overloading; having methods that have the same
            // name but different parameters

            //Book full = new Book();
            
            //full.fullThrottle();


            

            

            /*
            // A static attrbute belongs to the class and not the object of the class

            
            Book half = new Book();
            // half.halfThrottle();  // this does not work because the static method halfthrottle doesnt belong to the new object half but the class book

            // A static attrbute belongs to the class and not the object of the class

            */

            Book.halfThrottle(); // this runs because the half throttle static method because it refrences the book class and not any object

            /*
            Person myObj = new Person();

            myObj.Name = "liam";
            myObj.Job = "software";

            Console.WriteLine(myObj.Name);
            Console.WriteLine(myObj.Job);
            */

            Person myObj = new Person();
            var dara = myObj.name;
            Zombies myZombie = new Zombies();
            
            myZombie.Sound();


            //Person myPerson = new Person();
            //myPerson.Honk();


            Person myBaby = new Baby();

            myBaby.Sound();


            //abstract.cs

            Pig pig = new Pig();

            pig.occupation();

            // interfaces

            //iAnimal animal = new iAnimal(); /// CANNOT INSTANCIATE AN INTERFACE
            Goat myGoat = new Goat();
            myGoat.getSum();
        }




    }
}
