﻿using System;
using System.Collections.Generic;
using System.Text;

namespace csharpPractice
{
    // classes are blueprints for creating objects
    class Book
    {
        public string Title;
        public string Author;
        public string Rating;

      

        // A STATIC ATTRIBUTE IS THE SAME FOR ALL OF THE OBJECTS OF THE CLASS, A static attrbute belongs to the class and not the object of the class

        public static int Song = 2;


        public string Model = "success";


        //constructors: special methods thst sre put in csharp classes that get called when we create an object of the class
        // constructors are methods of a class that is called automatically when a new instance of a class is created, it is the ame name as the class name;

        
        public Book(string aTitle, string aAuthor,string aRating)
        {
             Title = aTitle;
            Author = aAuthor;
            Rating = aRating;
        }

        /*
        
        // get a property and set it to a particular value or standard;

        public  string Authors {
            get { return Author; }
            set { 

                if(value == "jp morgan" || value == "chimamanda adichie")
                {
                    Author = value;
                }
                else
                {
                    Author = "N/A";
                }
            } 
        }


        

        */
       



        
        public void fullThrottle()
        {
            Console.WriteLine("this is a full throttle");
        }

        // A static attrbute belongs to the class and not the object of the class

        static public void halfThrottle()
        {
            Console.WriteLine("this is a half throttle");
        }

         public void getName()
        {
            Console.WriteLine("my name is dara");
        }

    }

    
}
