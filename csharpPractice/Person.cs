﻿using System;
using System.Collections.Generic;
using System.Text;

namespace csharpPractice
{
    class Person // if you dont want other classes to inherit from a class, use the sealed keyword e.g sealed class Person
    {
        public string name = "success";
        private string job = "programmer";

        /*
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        */
        /*
        public string Name { get; set; }
        public string Job { get; set; }
        */

       

        public void Honk()
        {
            Console.WriteLine("honk honk");
        }

        public void Sound()
        {
            Console.WriteLine("the person makes a sound");
        }

    }

    class Baby: Person
    {
        public void Sound()
        {
            Console.WriteLine("the baby makes a sound");
        }
    }

    class Adult : Person
    {
        public void Sound()
        {
            Console.WriteLine("the adult makes a sound"); ;
        }
    }


    // AbstractPractice myAbs = new AbstractPractice(); // cannot create instance of an abvstract class /./ error




    
}
