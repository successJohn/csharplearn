﻿using System;
using System.Collections.Generic;
using System.Text;

namespace csharpPractice
{
    abstract class AbstractPractice  // an abstract class cannot be inherited nor instanciated
    {
        abstract public void occupation();
        abstract public void gender();
        // an abstract method cannot have a body. the body is derived from the inherited class.
    }

    class Pig : AbstractPractice
    {
        public override void occupation()
        {
            Console.WriteLine("she is a doctor");
        }

        public override void gender()
        {
            Console.WriteLine("she is a female");
        }


    }
}